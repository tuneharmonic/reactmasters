export const CREATE_BLOG = 'CREATE_BLOG';
export type CREATE_BLOG = typeof CREATE_BLOG;

export const ADD_POST = 'ADD_POST';
export type ADD_POST = typeof ADD_POST;

export const GET_BLOG = 'GET_BLOG';
export type GET_BLOG = typeof GET_BLOG;