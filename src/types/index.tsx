export interface IStoreState {
    blogs: IBlog[];
    posts: IBlogPost[];
}

export interface IBlog {
    id: number;
    author: string;
    title: string;
}

export interface IBlogPost {
    id: number;
    blogId: number;
    title: string;
    content: string;
    timestamp: number;
}