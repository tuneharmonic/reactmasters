import * as React from 'react';
import './App.css';
import BlogContainer from './containers/BlogContainer';
import BlogListContainer from './containers/BlogListContainer';

import { NavLink, Route, Switch } from 'react-router-dom';


class App extends React.Component {
  public title = "Kyle's Blog App";

  public render() {
    return (
      <div className="App">
        <div className="App-header">
          <div className="App-title">{this.title}</div>
          <NavLink exact={true} className="navLink" activeClassName="active" to="/">Home</NavLink>
        </div>
        <Switch>
          <Route path="/blog/:id" component={BlogContainer} />
          <Route component={BlogListContainer} />
        </Switch>
      </div>
    );
  }
}

export default App;
