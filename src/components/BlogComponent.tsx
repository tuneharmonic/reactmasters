import * as React from 'react';
import './BlogComponent.css';

import { IBlog, IBlogPost } from '../types';
import BlogPostForm from './BlogPostForm';
import PostComponent from './PostComponent';

interface IPostPrimitive {
    title: string;
    content: string;
}

interface IProps {
    addPost: (id: number, title: string, content: string) => void;
    getBlog: (blogId: number) => IBlog;
    getPosts: (blogId: number) => IBlogPost[];
    match: any;
}

class BlogComponent extends React.Component<IProps> {
    private blog: IBlog;
    private posts: IBlogPost[];
    private blogId: number;

    constructor(props) {
        super(props);
        this.submit = this.submit.bind(this);

        const { getBlog, match } = this.props;
        this.blogId = match.params.id;
        this.blog = getBlog(this.blogId) || { id: 0, author: '', title: '' };
    }

    public submit(post: IPostPrimitive) {
        if (this.validatePost(post)) {
            this.props.addPost(this.blogId, post.title, post.content);
        }
    }

    public validatePost(post: IPostPrimitive): boolean {
        return post.content !== undefined && post.content !== null && post.content.trim() !== ''
            && post.title !== undefined && post.title !== null && post.title.trim() !== '';
    }

    public render() {
        const { getPosts } = this.props;
        this.posts = getPosts(this.blogId).reverse() || [];

        return (
            <div className="blogComponent">
                <BlogPostForm onSubmit={this.submit} />
                <h1>{this.blog.title}</h1>
                { this.posts.map(post =>
                        <PostComponent key={post.id} author={this.blog.author} post={post} />)}
            </div>
        );
    }
}

export default BlogComponent;