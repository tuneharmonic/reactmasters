import * as React from 'react';
import { Field, InjectedFormProps, reduxForm } from 'redux-form';

import './BlogListForm.css';

class BlogListForm extends React.Component<InjectedFormProps> {

    public render() {
        const { handleSubmit } = this.props;
        return (
            <form onSubmit={handleSubmit}>
                <p>Create a new blog:</p>
                <div>
                    <label htmlFor="title">Title</label>
                    <Field name="title" component="input" type="text" />
                </div>
                <div>
                    <label htmlFor="author">Author</label>
                    <Field name="author" component="input" type="text" />
                </div>
                <button type="submit">Submit</button>
            </form>
        );
    }
}

export default reduxForm({
    form: 'blogForm'
})(BlogListForm);
