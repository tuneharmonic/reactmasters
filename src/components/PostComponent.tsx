import * as React from 'react';
import { IBlogPost } from '../types';

import './PostComponent.css';

interface IProps {
    author: string;
    post: IBlogPost;
}

class PostComponent extends React.Component<IProps> {
    public fullDateString(date: Date): string {
        const day = date.getDate();
        const month = date.getMonth() + 1;
        const year = date.getFullYear();
        const minute = date.getMinutes();

        let meridian = 'AM';
        let hour = date.getHours();
        if (hour >= 12) {
            meridian = 'PM';
        }
        if (hour === 0) {
            hour = 12;
        }
        if (hour > 12) {
            hour -= 12;
        }

        const formatMinute = minute < 10 ? `0${minute}` : minute.toString();

        return `on ${month}/${day}/${year} at ${hour}:${formatMinute} ${meridian}`;
    }

    public render() {
        const {author, post} = this.props;
        const postDate = new Date(post.timestamp);

        return (
            <div className="postComponent">
                <h3>{post.title}</h3>
                <p>By {author} - {this.fullDateString(postDate)}</p>
                <p>{post.content}</p>
            </div>
        );
    }
}

export default PostComponent;