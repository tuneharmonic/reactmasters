import * as React from 'react';
import { Link } from 'react-router-dom';

import { IBlog } from '../types';
import BlogListForm from './BlogListForm';

import './BlogListComponent.css';

interface IProps {
    addBlog: (author: string, title: string) => void;
    myBlogs: IBlog[]
}

interface IBlogPrimitive {
    title: string;
    author: string;
}

class BlogListComponent extends React.Component<IProps> {

    constructor(props) {
        super(props);
        this.submit = this.submit.bind(this);
    }

    public submit(newBlog: IBlogPrimitive): void {
        if (this.validateNewBlog(newBlog)) {
            this.props.addBlog(newBlog.author, newBlog.title);
        }
    }

    public validateNewBlog(newBlog: IBlogPrimitive): boolean {
        const titleValid = newBlog.title !== undefined && newBlog.title !== null && newBlog.title !== '';
        const authorValid = newBlog.author !== undefined && newBlog.author !== null && newBlog.author !== '';

        return titleValid && authorValid;
    }

    public render() {
        const { myBlogs } = this.props;

        return (
            <div className="blogListComponent">
            <BlogListForm onSubmit={this.submit} />
            <p>View current blogs:</p>
                { myBlogs.map(blog => {
                    return (
                        <div className="blogLink" key={blog.id}>
                            <Link to={`/blog/${blog.id}`}>{blog.title} by {blog.author}</Link>
                        </div>
                    );
                })}
            </div>
        );
    }
}

export default BlogListComponent;