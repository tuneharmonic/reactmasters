import * as React from 'react';
import { Field, InjectedFormProps, reduxForm } from 'redux-form';

import './BlogPostForm.css';

class BlogPostForm extends React.Component<InjectedFormProps> {

    public render() {
        const { handleSubmit } = this.props;
        return (
            <form onSubmit={handleSubmit}>
                <p>Create a new blog post:</p>
                <div>
                    <label htmlFor="title">Title</label>
                    <Field name="title" component="input" type="text" />
                </div>
                <div>
                    <label htmlFor="">Content</label>
                    <Field name="content" component="textarea" />
                </div>
                <button type="submit">Submit</button>
            </form>
        );
    }
}

export default reduxForm({
    form: 'postForm'
})(BlogPostForm);
