import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import * as actions from '../actions';
import BlogListComponent from '../components/BlogListComponent';

const mapStateToProps = (state) => {
    return {
        myBlogs: state.blog.blogs
    };
}

const mapDispatchToProps = (dispatch: Dispatch<actions.BlogAction>) => {
    return {
        addBlog: (author: string, title: string) => {
            dispatch(actions.createBlog(author, title));
        }
    };
};

const BlogListContainer = connect(mapStateToProps, mapDispatchToProps)(BlogListComponent);

export default BlogListContainer;