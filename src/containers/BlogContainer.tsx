import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import * as actions from '../actions';
import BlogComponent from '../components/BlogComponent';

// const mapStateToProps = (state) => {
//     return {
//         myPosts: [...state.blog.posts].filter(p => p)
//     };
// };

const mapStateToProps = (state) => {
    return {
        getBlog: (blogId: number) => {
            return state.blog.blogs.find(b => +b.id === +blogId);
        },
        getPosts: (blogId: number) => {
            return state.blog.posts.filter(p => +p.blogId === +blogId)
        }
    };
}

const mapDispatchToProps = (dispatch: Dispatch<actions.BlogAction>) => {
    return {
        addPost: (id: number, title: string, content: string) => {
            dispatch(actions.addPost(id, title, content));
        }
    };
};

const BlogContainer = connect(mapStateToProps, mapDispatchToProps)(BlogComponent);

export default BlogContainer;