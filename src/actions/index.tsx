import * as constants from '../constants';

export interface ICreateBlogAction {
    type: constants.CREATE_BLOG;
    author: string;
    title: string;
}

export interface IAddPostAction {
    type: constants.ADD_POST;
    blogId: number;
    title: string;
    content: string;
}

export type BlogAction = ICreateBlogAction | IAddPostAction;

export function createBlog(author: string, title: string): ICreateBlogAction {
    return {
        author,
        title,
        type: constants.CREATE_BLOG
    };
}

export function addPost(blogId: number, title: string, content: string): IAddPostAction {
    return {
        blogId,
        content,
        title,
        type: constants.ADD_POST
    };
}