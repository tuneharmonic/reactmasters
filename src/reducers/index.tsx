import { BlogAction } from '../actions';
import { ADD_POST, CREATE_BLOG } from '../constants';
import { IStoreState } from '../types';

const initState: IStoreState = {
    blogs: [{
        author: 'Kyle Johnson',
        id: 1,
        title: 'Kyle\'s blog!'
    }],
    posts: [{
        blogId: 1,
        content: 'Yes!',
        id: 1,
        timestamp: Date.now(),
        title: 'This totally works!'
    }]
};

export function blog(state: IStoreState = initState, action: BlogAction): IStoreState {
    switch (action.type) {
        case CREATE_BLOG:
            return { 
                ...state, 
                blogs: [
                    ...state.blogs, 
                    { 
                        author: action.author,
                        id: state.blogs.length + 1,
                        title: action.title,
                    }
                ]
            };
        case ADD_POST:
            return { 
                ...state, 
                posts: [
                    ...state.posts, 
                    {
                        blogId: action.blogId,
                        content: action.content,
                        id: state.posts.length + 1,
                        timestamp: Date.now(),
                        title: action.title
                    }
                ]
            };
        default:
            return {...state};
    }
}